BUILD_DIR = build
SRC_DIR = src
ENV_OBJS = $(BUILD_DIR)/fish.o  \
			$(BUILD_DIR)/env.o 
UI_OBJS = $(BUILD_DIR)/main.o
AI_HEADERS = $(wildcard AIs/*.h)
CXX_FLAGS = -pg #-L ncurses/build/lib/
LIBS = -lncurses -ldl

$(BUILD_DIR)/%.o : $(SRC_DIR)/%.cpp
	g++ -c $< -g -pg -O2 -I $(SRC_DIR) -o $@

.PHONY: all

all :  $(AI_HEADERS) $(ENV_OBJS) $(UI_OBJS)
	$(MAKE) -C AIs/
	g++ -o $(BUILD_DIR)/main AIs/*.o $(ENV_OBJS) $(UI_OBJS) $(CXX_FLAGS) $(LIBS)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/fish.o: $(SRC_DIR)/fish.h | $(BUILD_DIR)
$(BUILD_DIR)/env.o: $(SRC_DIR)/env.h | $(BULID_DIR)
$(BUILD_DIR)/main.o: $(SRC_DIR)/main.cpp $(SRC_DIR)/ai.h | $(BUILD_DIR)
	g++ -c $< -g -pg -O2 -I ncurses/build/include/ -I $(SRC_DIR) -o $@

clean :
	$(MAKE) clean -C AIs/
	rm -r $(BUILD_DIR)

run :
	$(BUILD_DIR)/main

db:
	gdb main
