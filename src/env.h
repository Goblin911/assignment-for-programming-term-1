#ifndef ENV_H
#define ENV_H

#include "consts.h"
#include "fish.h"
#include <cstring>

typedef int const (*Map_t)[MAP_MAXC];
typedef void (*CallbackBefore_t)(const int* player_list, int pos_now);
/**
 * This is a typedef of a function pointer which accepts an array of integer 
 * that figures out the order of this turn and an integer for the current player id.
 */
typedef void (*CallbackMove_t)(int player_id, int from_x, int from_y, bool flag);
typedef void (*CallbackAttack_t)(int player_id, int des_x, int des_y, int flag);
typedef void (*CallbackRevive_t)(int player_id);
typedef void (*CallbackIncrease_t)(int player_id, int type, bool flag);
typedef void (*CallbackLevelUp_t)(int player_id);

class Environment;
class fish;

class FishInfo {

    /**
     * Stores the status of a fish, which can be used for game board displaying, etc.
     * This class from which controller class 'FishMeta' inherited is made in order to
     * preventing the external call from damaging the internal data.
     */

    public:
        int pos_x, pos_y, id;
        int strength, speed, level;
        int max_health, cur_health;
        int point, exp, jump_sum;
        int score; // only for display

        int status; // init, dead or alive
        bool attacked, moved;
}; 

class FishMeta : public FishInfo {

    /**
     * The controller class which is in responsiability for communicating with 
     * the 'Environment' instance as well as calling the functions of AIs. 
     * (The 'env' pointer leads to the environment, and 'ctl' leads to the AI instance.)
     *
     * When the AI needs to do action, it will invoke the functions below which will
     * result in calling functions of the corresponding instance of 'Environment'.
     *
     * The instance of 'Environment' will use the pointers of this class to represent
     * the players, and it would be convenient to trigger AIs' movement by calling 'ai_*' function.
     *
     * In a word, this class just simply builds up a bridge for AI 'fish' class and the 'Environment' class.
     */

    public:
        Environment *env;   // reflective pointer
        fish *ctl; // pointer to trigger the AI

        int get_content(int x, int y);
        int get_hp(int id);
        int get_total_player();

        bool move(int new_x, int new_y);
        bool attack(int des_x, int des_y);

        bool increase_health();
        bool increase_strength();
        bool increase_speed();

        void level_up();
        void clear();

        void ai_init();
        void ai_revive(int &x, int &y);
        void ai_play();

        int get_score() const;

}; // store the metadata of a fish, which is made use of the Environment

class Environment {

    /**
     * The model class that holds up the entire gaming enviroment.
     * It stores all the game data including pointers.
     * Call 'game_start()' to initialize the game and call 'turn()' to make the game move on.
     */

    private:
        struct Pos {
            int x, y;
            Pos() {}
            Pos(int _x, int _y) : x(_x), y(_y) {}
        }; // the paired int to represent the location in a map

        int map[MAP_MAXR][MAP_MAXC];
        int player_cnt, pending_cnt, turn_cnt;
        FishMeta *players[MAX_PLAYER];

        CallbackBefore_t cb_before;
        CallbackMove_t cb_move;
        CallbackAttack_t cb_attack;
        CallbackRevive_t cb_revive;
        CallbackIncrease_t cb_increase;
        CallbackLevelUp_t cb_level_up;

        Pos random_blank();
        bool check_blank(int x, int y); 
        bool check_alive(FishMeta *fptr);
        bool inside_map(int x, int y);

        void place_fish(FishMeta *player, int x, int y);
        void clear_position(int x, int y);
        void refresh_food();

        /**
         *  The functions below are made for FishMeta.
         */

        friend class FishMeta;
        bool try_attack(FishMeta *fptr, int x, int y);
        bool try_move(FishMeta *fptr, int new_x, int new_y);
        int get_content(int x, int y);
        int get_hp(int id);

    public:
        /**
         * The following functions can be made use of by UI. 
         */

        Environment();
        ~Environment();

        void add_player(fish *ptr);
        void game_start();
        void turn();

        void set_callback_before(CallbackBefore_t cb);
        void set_callback_move(CallbackMove_t cb);
        void set_callback_attack(CallbackAttack_t cb);
        void set_callback_revive(CallbackRevive_t cb);
        void set_callback_increase(CallbackIncrease_t cb);
        void set_callback_level_up(CallbackLevelUp_t cb);

        FishInfo get_info(int id);
        Map_t get_map();
        int get_player_num();
};

#endif
