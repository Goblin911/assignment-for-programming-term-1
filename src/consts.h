#ifndef CONSTS_H
#define CONSTS_H

////////// Official Constants ////////////////

const int N = 10; //地图横坐标范围1-N
const int M = 10; //地图纵坐标范围1-M
const int MAX_FOOD = 50; //刷新食物数量
const int FOOD_ROUND = 5; //刷新食物回合数
const int EMPTY = 0; //格子为空
const int FOOD = -1; //食物
const int LEVEL_POINT = 3; //升级能获得的点数

//////////////////////////////////////////////

const int MAX_LEVEL = 1000;
const int MAP_MAXR = N;
const int MAP_MAXC = M;
const int MAX_PLAYER = 40;
const int PLACE_TYPE_BLANK = 0;
const int PLACE_TYPE_FOOD = 1;
const int PLACE_TYPE_INVALID = 0x7fffffff;

const int PLAYER_INIT_POINT = 10;
const int PLAYER_STATUS_REVIVING = -1;
const int PLAYER_STATUS_ALIVE = 1;
const int PLAYER_STATUS_DEAD = 0;
const int PLAYER_NOT_ALLOWED = 0;

const int INCREASE_TYPE_HEALTH = 0;
const int INCREASE_TYPE_STRENGTH = 1;
const int INCREASE_TYPE_SPEED = 2;

#endif
