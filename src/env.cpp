#include "env.h"
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <cassert>

using std::max;
using std::min;
using std::random_shuffle;
using std::sort;

Environment::Environment() {
    player_cnt = 0;
    pending_cnt = 0;
    turn_cnt = 0;
    memset(map, 0, sizeof map);
}

Environment::~Environment() {
    for (int i = 0; i < player_cnt; i++)
        delete players[i];
}

void Environment::clear_position(int x, int y) {
    map[x][y] = PLACE_TYPE_BLANK;
}

void Environment::place_fish(FishMeta *player, int x, int y) {
    player -> pos_x = x;
    player -> pos_y = y;
    map[x][y] = -player -> id;
}

bool Environment::check_alive(FishMeta *fptr) {
    if (fptr -> cur_health > 0) return true; // still alive

    clear_position(fptr -> pos_x, fptr -> pos_y);
    fptr -> status = PLAYER_STATUS_DEAD;
    return false;
}

bool Environment::try_attack(FishMeta *fptr, int x, int y) {

    int flag;
    if (!inside_map(x, y) || 
            abs(x - fptr -> pos_x) + abs(y - fptr -> pos_y) != 1 ||
            fptr -> strength == 0)
        flag = PLACE_TYPE_INVALID; // not allowed

    else if (fptr -> attacked)
        flag = PLACE_TYPE_INVALID; // has already attacked
    else if (map[x][y] == PLACE_TYPE_BLANK)
        flag = PLACE_TYPE_INVALID; // nothing to attack
    else
    {
        fptr -> attacked = true;
        flag = map[x][y];
        if (map[x][y] == PLACE_TYPE_FOOD)
        {
            int max_hp = fptr -> max_health;
            int cur_hp = fptr -> cur_health;
            fptr -> cur_health = min(max_hp, cur_hp + max(2, max_hp / 10));
            fptr -> exp += 1;
            clear_position(x, y);
        }
        else
        {
            FishMeta *opp = players[-map[x][y] - 1];
            opp -> cur_health -= fptr -> strength;
            if (!check_alive(opp))
            {
                if (fptr -> level < opp -> level)
                    fptr -> jump_sum += opp -> level - fptr -> level;
                fptr -> exp += max(1, opp -> level / 2);
            }
        }
        fptr -> level_up();
    }
    if (cb_attack) cb_attack(fptr -> id, x, y, flag);
    return flag != PLACE_TYPE_INVALID;
}

bool Environment::try_move(FishMeta *fptr, int new_x, int new_y) {
    bool flag;
    int from_x = fptr -> pos_x, from_y = fptr -> pos_y;

    if (fptr -> moved || fptr -> attacked) flag = false;
    // cannot move after the attack
    else if (!inside_map(new_x, new_y) || 
            abs(new_x - fptr -> pos_x) + abs(new_y - fptr -> pos_y) > fptr -> speed ||
            map[new_x][new_y] != PLACE_TYPE_BLANK)
        flag = false; // not feasible
    else
    {
        clear_position(fptr -> pos_x, fptr -> pos_y);
        place_fish(fptr, new_x, new_y);
        fptr -> moved = true;
        flag = true; // successfull
    }
    if (cb_move) cb_move(fptr -> id, from_x, from_y, flag);
    return flag;
}

int Environment::get_content(int x, int y) {
    if (!inside_map(x, y)) return PLACE_TYPE_INVALID;
    return map[x][y];
}

int Environment::get_hp(int id) {
    if (!(1 <= id && id <= player_cnt))
        return PLAYER_NOT_ALLOWED;
    return players[id - 1] -> cur_health;
}

void Environment::add_player(fish *ptr) {
    FishMeta *np = new FishMeta;
    players[player_cnt++] = np;
    np -> ctl = ptr;
    np -> env = this;
    ptr -> fmptr = np;
    ptr -> setID(player_cnt);
}

void Environment::refresh_food() {
    static Pos avail[MAP_MAXR * MAP_MAXC];
    int acnt = 0;
    for (int i = 0; i < MAP_MAXR; i++)
        for (int j = 0; j < MAP_MAXC; j++)
        {
            if (map[i][j] < 0) continue;
            if (map[i][j] == PLACE_TYPE_FOOD) 
                map[i][j] = PLACE_TYPE_BLANK;
            avail[acnt++] = Pos(i, j);
        }

    random_shuffle(avail, avail + acnt);
    int size = min(MAX_FOOD, acnt);
    for (int i = 0; i < size; i++)
        map[avail[i].x][avail[i].y] = PLACE_TYPE_FOOD;
}

Environment::Pos Environment::random_blank() {
    int x, y;
    do x = rand() % MAP_MAXR, 
        y = rand() % MAP_MAXC;
    while (map[x][y] != PLACE_TYPE_BLANK);
    return Pos(x, y);
}

bool Environment::inside_map(int x, int y) {
    return (0 <= x && x < MAP_MAXR &&
            0 <= y && y < MAP_MAXC);
}

bool Environment::check_blank(int x, int y) {
    if (!inside_map(x, y)) return false;
    return map[x][y] == PLACE_TYPE_BLANK;
}

void Environment::game_start() {
    for (int i = 0; i < player_cnt; i++)
    {
        FishMeta *player = players[i];
        player -> clear();

        Pos p = random_blank();
        place_fish(players[i], p.x, p.y); // place the player randomly
        player -> status = PLAYER_STATUS_REVIVING;
        player -> ai_init();
        if (check_alive(player))
            player -> status = PLAYER_STATUS_ALIVE;
    }
}

Map_t Environment::get_map() {
    return map;
}

const double EPS = 1e-8;

int dcmp(double x) {
    if (fabs(x) < EPS) return 0;
    return x < 0 ? -1 : 1;
}

bool cmp(const FishMeta *a, const FishMeta *b) {
    if (a -> speed != b -> speed) 
        return a -> speed > b -> speed;
    double rate_a = a -> cur_health / (double)a -> max_health;
    double rate_b = b -> cur_health / (double)b -> max_health;
    int sgn = dcmp(rate_a - rate_b);
    if (sgn != 0) return sgn > 0;
    return a -> get_score() < b -> get_score();
}

void Environment::turn() {

    static FishMeta *player_list[MAX_PLAYER];
    static int order[MAX_PLAYER];

    for (int i = 0; i < player_cnt; i++)
        player_list[i] = players[i];
    sort(player_list, player_list + player_cnt, cmp);
    for (int i = 0; i < player_cnt; i++)
        order[i] = player_list[i] -> id;


    if (turn_cnt % FOOD_ROUND == 0) refresh_food();
    for (int i = 0; i < player_cnt; i++)
    {
        FishMeta *player = player_list[i];
        if (player -> status == PLAYER_STATUS_REVIVING)
        {
            // it's time to revive!
            int x, y;
            player -> ai_revive(x, y);
            if (!check_blank(x, y))
            {
                Pos p = random_blank();
                x = p.x;
                y = p.y;
            }
            place_fish(player, x, y);
            player -> status = PLAYER_STATUS_ALIVE;
            if (cb_revive) cb_revive(player -> id);
            // becomes alive again
        }
    }

    for (int i = 0; i < player_cnt; i++)
    {
        FishMeta *player = player_list[i];
        if (player -> status == PLAYER_STATUS_ALIVE)
        {
            player -> attacked = false;
            player -> moved = false;
            player -> ai_play();
            if (cb_before) cb_before(order, i);
        }
        else if (player -> status == PLAYER_STATUS_DEAD)
            player -> status = PLAYER_STATUS_REVIVING;
        //the dead player will revive in the next turn
    }

    turn_cnt++;
}

FishInfo Environment::get_info(int id) {
    if (!(1 <= id && id <= player_cnt)) return FishInfo(); // not found
    FishInfo res = *players[id - 1];
    res.score = players[id - 1] -> get_score();
    return res;
}

void Environment::set_callback_before(CallbackBefore_t cb) {
    cb_before = cb;
}

void Environment::set_callback_move(CallbackMove_t cb) {
    cb_move = cb;
}

void Environment::set_callback_attack(CallbackAttack_t cb) {
    cb_attack = cb;
}

void Environment::set_callback_revive(CallbackRevive_t cb) {
    cb_revive = cb;
}

void Environment::set_callback_level_up(CallbackLevelUp_t cb) {
    cb_level_up = cb;
}

void Environment::set_callback_increase(CallbackIncrease_t cb) {
    cb_increase = cb;
}

int Environment::get_player_num() {
    return player_cnt;
}

bool FishMeta::move(int new_x, int new_y) {
    if (status != PLAYER_STATUS_ALIVE)
        return false;
    return env -> try_move(this, new_x, new_y);
}

bool FishMeta::attack(int des_x, int des_y) {
    if (status != PLAYER_STATUS_ALIVE)
        return false;
    return env -> try_attack(this, des_x, des_y);
}

int FishMeta::get_content(int x, int y) {
    if (status != PLAYER_STATUS_ALIVE)
        return PLAYER_NOT_ALLOWED;
    int t = env -> get_content(x, y);
    if (t == PLACE_TYPE_INVALID) return PLAYER_NOT_ALLOWED;
    if (t == PLACE_TYPE_FOOD) return FOOD;
    if (t == PLACE_TYPE_BLANK) return EMPTY;
    return -t; // the player's id
}

bool FishMeta::increase_health() {
    bool flag;
    if (!point) flag = false; // no point left
    else
    {
        point--; 
        cur_health += 2;
        max_health += 2;
        flag = true;
    }
    if (env -> cb_increase) 
        env -> cb_increase(id, INCREASE_TYPE_HEALTH, flag);
    return flag;
}

bool FishMeta::increase_strength() {
    bool flag;
    if (!point) flag = false; // no point left
    else
    {
        point--;
        strength++;
        flag = true;
    }
    if (env -> cb_increase) 
        env -> cb_increase(id, INCREASE_TYPE_STRENGTH, flag);
    return flag;
}

bool FishMeta::increase_speed() {
    bool flag;
    if (!point) flag = false; // no point left
    else
    {
        point--;
        speed++;
        flag = true;
    }
    if (env -> cb_increase) 
        env -> cb_increase(id, INCREASE_TYPE_SPEED, flag);
    return flag;
}

void FishMeta::level_up() {
    int new_level; 
    int l, r, mid;
    for (l = 1, r = MAX_LEVEL; mid = (l + r) >> 1, r - l > 1;
            (((mid + 2) * (mid - 1) / 2 <= exp) ? l : r) = mid);
    new_level = l;

    assert(new_level >= level);
    point += (new_level - level) * LEVEL_POINT;
    if (new_level > level && env -> cb_level_up) 
        env -> cb_level_up(id);

    level = new_level;
}

void FishMeta::clear() {
    strength = 0;
    speed = 0;
    cur_health = max_health = 0;
    level = 1;
    exp = 0;
    point = PLAYER_INIT_POINT;
    jump_sum = 0;
}

int FishMeta::get_score() const {
    return exp + 2 * jump_sum;
}

void FishMeta::ai_init() {
    ctl -> init();
}

void FishMeta::ai_revive(int &x, int &y) {
    cur_health = max(max_health / 10, 1);
    ctl -> revive(x, y);
    x--, y--;
}

void FishMeta::ai_play() {
    ctl -> play();
}

int FishMeta::get_hp(int id) {
    if (status != PLAYER_STATUS_ALIVE)
        return PLAYER_NOT_ALLOWED;
    return env -> get_hp(id);
}

int FishMeta::get_total_player() {
    return env -> get_player_num();
}
